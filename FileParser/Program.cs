﻿using System;
using System.IO;
using System.Collections.Generic;

namespace AplhaFx.FileParserTestHarness
{
    // This class is the entry point, a client / caller, if the remaining classes were put in a Class Library
    public class Program
    {
        public static void Main(string[] args)
        {
            FileParser objFP = new FileParser();

            try
            {
                string folderPath = string.Format("{0}{1}TestData{1}",new DirectoryInfo(Path.GetDirectoryName(Environment.GetCommandLineArgs()[0])).Parent.Parent.Parent.FullName, Path.DirectorySeparatorChar);

                // Uncomment the following one by one to see the difference between the various modes for the same file.
                //List<WordOccurence> lstWords = objFP.GetMostUsedWordsFromFile(string.Format("{0}{1}",folderPath,"BehaviourTest.txt"), 3, StringComparer.CurrentCultureIgnoreCase, TopMode.Strict);
                //List<WordOccurence> lstWords = objFP.GetMostUsedWordsFromFile(string.Format("{0}{1}",folderPath,"BehaviourTest.txt"), 3, StringComparer.CurrentCultureIgnoreCase, TopMode.Fair);
                //List<WordOccurence> lstWords = objFP.GetMostUsedWordsFromFile(string.Format("{0}{1}",folderPath,"BehaviourTest.txt"), 3, StringComparer.CurrentCultureIgnoreCase, TopMode.Generous);

                // Use the following for case-insensitive search
                List<WordOccurence> lstWords = objFP.GetMostUsedWordsFromFile(string.Format("{0}{1}",folderPath,"The+Lord+Of+The+Ring+1-The+Fellowship+Of+The+Ring_djvu_txt.txt"), 10, StringComparer.CurrentCultureIgnoreCase);

                // Use the following for case-sensitive search
                //List<WordOccurence> lstWords = objFP.GetMostUsedWordsFromFile(string.Format("{0}{1}",folderPath,"The+Lord+Of+The+Ring+1-The+Fellowship+Of+The+Ring_djvu_txt.txt"), 10, StringComparer.CurrentCulture);

                // Use the following line if you are using a local file
                //List<WordOccurence> lstWords = objFP.GetMostUsedWordsFromFile(@"C:\MyFolder\Samplefile.txt", 10, StringComparer.CurrentCultureIgnoreCase);

                if (lstWords == null || lstWords.Count == 0)
                {
                    Console.WriteLine("No words found in the file, possibly an empty file or file with only white space characters.");
                }
                else
                {
                    foreach (WordOccurence objWO in lstWords)
                    {
                        Console.WriteLine(string.Format("Order : {0}, Rank : {1}, Dense Rank : {2}, Occurences : {3}, Word : {4}", objWO.Order, objWO.Rank, objWO.DenseRank, objWO.Occurence, objWO.Word));
                    }
                }
            }
            catch(Exception exc)
            {
                Console.WriteLine(exc.Message);
                while (exc.InnerException != null)
                {
                    exc = exc.InnerException;
                    Console.WriteLine(exc);
                }
            }

            Console.Read();
        }
    }
}
