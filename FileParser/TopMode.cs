﻿namespace AplhaFx
{
    // This enumeration contains how many top (n?) words should be returned, when there are other words with the same occurence as the nth word
    public enum TopMode
    {
        Strict = 0,
        Fair = 1,
        Generous = 2
    };
}
