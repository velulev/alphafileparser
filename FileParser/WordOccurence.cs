﻿namespace AplhaFx
{
    // This class is a simple POCO object for returning the result with all necessary information
    public class WordOccurence
    {
        protected int _order = 0;
        protected int _rank = 0;
        protected int _denseRank = 0;
        protected int _occurence = 0;
        protected string _word = null;

        public WordOccurence(int order, int rank, int denseRank, int occurence, string word)
        {
            _order = order;
            _rank = rank;
            _denseRank = denseRank;
            _occurence = occurence;
            _word = word;
        }

        public int Order { get { return _order; } }
        public int Rank { get { return _rank; } }
        public int DenseRank { get { return _denseRank; } }
        public int Occurence { get { return _occurence; } }
        public string Word { get { return _word; } }
    }
}
