﻿using System;
using System.IO;
using System.Runtime;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace AplhaFx
{
    // This class does all the core word counting logic
    public class FileParser
    {
        public List<WordOccurence> GetMostUsedWordsFromFile(string fileName, int topLimit, StringComparer comparer, TopMode mode = TopMode.Fair)
        {
            if (!File.Exists(fileName))
            {
                throw new ArgumentException(String.Format("File '{0}' does not exist, please check the path, file name, extension and retry.", fileName), "fileName");
            }

            SortedList<string, Int32> slWordOccurences = GetFileTokens(fileName, comparer);
            SortedList<int, List<string>> slMostOccuringWords = GetTopWords(slWordOccurences, topLimit);
            TrimTopWordsForMode(slMostOccuringWords, topLimit, mode);
            return ConvertFlatResponse(slMostOccuringWords);
        }

        protected SortedList<string, Int32> GetFileTokens(string fileName, StringComparer comparer)
        {
            SortedList<string, Int32> slWordOccurences = new SortedList<string, Int32>(comparer);
            FileStream fileStream = new FileStream(fileName, FileMode.Open);
            using (StreamReader reader = new StreamReader(fileStream))
            {
                string line = null;
                while ((line = reader.ReadLine()) != null)
                {
                    //https://stackoverflow.com/questions/16725848/how-to-split-text-into-words
                    MatchCollection matches = Regex.Matches(line, @"\w+[^\s]*\w+|\w");

                    foreach (Match match in matches)
                    {
                        string word = match.Value.Trim();
                        if (word != string.Empty)
                        {
                            slWordOccurences[word] = slWordOccurences.ContainsKey(word) ? slWordOccurences[word] + 1 : 1;
                        }
                    }
                }
            }

            return slWordOccurences;
        }

        protected SortedList<int, List<string>> GetTopWords(SortedList<string, Int32> slstWordOccurences, int limit)
        {
            Comparer<int> descendingComparer = Comparer<int>.Create((x, y) => y.CompareTo(x));
            SortedList<int, List<string>> descWordsbyCount = new SortedList<int, List<string>>(limit, descendingComparer);
            //SortedList<int, List<string>> descWordsbyCount = new SortedList<int, List<string>>(limit); // If we want the ascending order list
            int lowestCount = -1;

            foreach (KeyValuePair<string, Int32> nv in slstWordOccurences)
            {
                if (lowestCount <= nv.Value) // || descWordsbyCount.Count < limit condition is not needed
                {
                    if (!descWordsbyCount.ContainsKey(nv.Value))
                    {
                        if (lowestCount > 0) descWordsbyCount.Remove(lowestCount);
                        descWordsbyCount[nv.Value] = new List<string>();
                        if (descWordsbyCount.Count == limit) lowestCount = descWordsbyCount.Keys[limit - 1];
                        //if (descWordsbyCount.Count == limit) lowestCount = descWordsbyCount.Keys[0]; // If we want the ascending order list
                    }

                    descWordsbyCount[nv.Value].Add(nv.Key);
                }
            }

            return descWordsbyCount;
        }

        protected bool TrimTopWordsForMode(SortedList<int, List<string>> occurencesWithWords, int topLimit, TopMode mode)
        {
            bool changesMade = false;
            if (mode == TopMode.Generous && occurencesWithWords.Count <= topLimit) return changesMade;

            int currentOccurence = -1, wordsSoFar = 0;

            foreach (KeyValuePair<int, List<string>> nv in occurencesWithWords)
            {
                currentOccurence++;
                if (wordsSoFar + nv.Value.Count > topLimit && mode == TopMode.Strict)
                {
                    nv.Value.RemoveRange(topLimit - wordsSoFar, nv.Value.Count - topLimit + wordsSoFar);
                    if (!changesMade) changesMade = true;
                }

                wordsSoFar = wordsSoFar + nv.Value.Count;
                if (wordsSoFar >= topLimit) break;
            }

            for (int i = occurencesWithWords.Count - 1; i > currentOccurence; i--)
            {
                occurencesWithWords.RemoveAt(i);
                if (!changesMade) changesMade = true;
            }

            return changesMade;
        }

        protected List<WordOccurence> ConvertFlatResponse(SortedList<int, List<string>> occurencesWithWords)
        {
            List<WordOccurence> lstWords = new List<WordOccurence>();

            int order = 1, rank = 1, denseRank = 1, prevOccurence = 0;

            foreach (KeyValuePair<int, List<string>> nv in occurencesWithWords)
            {
                foreach (string word in nv.Value)
                {
                    if (order > 1 && prevOccurence != nv.Key)
                    {
                        rank = order;
                        denseRank++;
                    }
                    lstWords.Add(new AplhaFx.WordOccurence(order, rank, denseRank, nv.Key, word));
                    prevOccurence = nv.Key;
                    order++;
                }
            }

            return lstWords;
        }
    }
}
