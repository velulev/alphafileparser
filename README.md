**Repository for the Coding Excercise given to Velu Mani**

This is a C# program to count the number of instances of each word in a text file, and to list the top 10 words along with the number of occurrences.

*The [sample file](https://archive.org/stream/TheLordOfTheRing1TheFellowshipOfTheRing/The+Lord+Of+The+Ring+1-The+Fellowship+Of+The+Ring_djvu.txt) for the excercise.*

Please change the full path of the file in the Program.cs file, to point to your local path to the file you want to test, before you run the program.

---

## Assumptions and Pointers :

1.	Newlines are definitely Word Delimiters, and a word must not go across multiple lines, a very common and obvious point, but better for me to be explicit.

2.	Based on the example of the exercise given, and the very definition of the problem of counting **words**, I have chosen a Regular Expression that safely parses words from each line in the text file, the primary advantages of this regular expression are, it considers the non-word separating non alpha numeric characters like – (co-operation), ‘ (don’t) and _ (my_name). The disadvantages being, it ignores the special characters like **$!@£%^&*()~**, and also some common word separators like comma (,)semi-colon (;), colon (:) and period (.). But as mentioned at the beginning of this point these are not necessary for word counting and can be safely ignored. But in case I have to do a similar program to count the tokens in a software program, then a different parsing mechanism can be used, either with or without Regular Expressions. Regular Expressions are not good for performance either, in which case a custom forward-only (as much as possible) parser will be easier to build, but is definitely not easy to do in the 2 hours allocated for this exercise. The regular expression was taken from [StackOverflow](https://stackoverflow.com/questions/16725848/how-to-split-text-into-words), and thoroughly tested for various scenarios using my local LinqPad.

3.	I have assumed it will be good for the program to count the words in both a case-sensitive and case-insensitive way, so, the option is a parameter (**StringComparer**) on the outermost function. I have left it as a parameter as this function is consumed locally from another .NET Core class, if the same function is to be exposed via Web API’s, then this parameter can be replaced with a **Boolean** variable named IgnoreCase, which when true can use **StringComparer.CurrentCultureIgnoreCase**, else **StringComparer.CurrentCulture** inside the function.

4.	I believe the objective of this exercise is about logical approach to problem solving (where I have considered only about decomposing functions, and no other design), than design (in which case, I would have added more classes with separate behavior, and also a ClassLibrary Project for the processing logic that is called by a Console Application, etc.,), performance (would have parallelized the word-counting processing while the program is reading the next line from the disk, either using threading or async functions) or coding standards (pretty much followed my thought process for the solution, and have not given much thought to the naming standards, and have only considered the readability and nothing else).

5.	I have also assumed it is pretty important to explain the multiple scenario case, where multiple Nth words have the same count, in which case, I have introduced 3 modes (as optional parameters, where the second  / Fair option is the default one), the 3 modes can be understood by requesting to search the top 3 words from the **BehaviourTest.txt** file in the **TestData** folder in the solution.

      1.	Strict (Stops at exactly the top words requested, even if there are more words with the same count as the last word).
      2.	Fair (May return more words than requested, if there are more words with the exact same count as the last top word i.e., same count as the 10th word, if top 10 words are to be counted).
      3.	Generous (Will return the Top n unique Word Counts and return each word with that count).

6.	I have kept the error handling to a minimum but reasonably tight for a 2 hour exercise, the only condition that is checked, is whether the file exists or not, and throw an Exception to be caught and dealt by the caller, and the remaining part of the method is not enclosed in a try block, because the code logic will not result in any Exception, and even if it does due to unexpected reasons can be caught and handled by the client.

7.	I have used simple program constructs to have full compatibility and support in .NET Core, and I developed it using the JetBrains Rider IDE on Mac OS to run on .NET Core 2.2, but my latest Mac update crashed, and I had to use Visual Studio Code on Windows to complete it, please let me know if you want any other Platform / IDE (project files), to make things easier, I have also kept it in a single solution with separate files (Program file (Program.cs) for Console App, acts as a client / caller, An enum file (TopMode.cs) to describe the various multiple scenarios, A Class file (WordOccurence.cs) to return the response as a simple POCO, which is easy for converting into JSON etc., when used in a web based API, and the other file (FileParser.cs) that does all the core processing).

8.	I have not given detailed comments in the Program to stick to the strict 2 hour duration, and I am happy to comment the code in a different branch, if you prefer.
